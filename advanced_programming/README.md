Advanced Python
===============

This directory contains a collection of practicals, each of which covers a
particular feature of the Python programming language. They are intended for
people who are familiar with the basics of Python, and want to learn about
some of the more advanced features of the language.

Practicals on the following topics are available. They can be viewed in any
order, but we recommend going through them in this order:

1. Function inputs and outputs (`function_inputs_and_outputs.ipynb`)
2. Modules and packages (`modules_and_packages.ipynb`)
3. Object-oriented programming (`object_oriented_programming.ipynb`)
4. Operator overloading (`operator_overloading.ipynb`)
5. Context managers (`context_managers.ipynb`)
6. Decorators (`decorators.ipynb`)
7. Threading and parallel processing (`threading.ipynb`)
8. Structuring projects (`structuring_projects.ipynb`)
